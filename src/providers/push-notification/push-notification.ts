import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AppConfig } from '../../app/config.constants';
import { PushOptions, PushObject, Push } from '@ionic-native/push';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class PushNotificationProvider {

  constructor(private push: Push, private storage: NativeStorage) { }

  eventId: string;
  notificationData: any;
  pushNotificationObject: PushObject;

  initializePushNotifications() {

    this.push.hasPermission()
      .then(async (res: any) => {

        if (res.isEnabled) {

          console.log('We have permission to send push notifications');
        } else {
          console.log('We do not have permission to send push notifications');
        }

      });

    // to initialize push notifications

    const options: PushOptions = {
      android: {
        senderID: AppConfig.firebaseSenderId
      },
      windows: {},
      browser: {
        pushServiceURL: AppConfig.pushServiceURL
      }
    };

    this.pushNotificationObject = this.push.init(options);

    this.pushNotificationObject.on('registration').subscribe(async (registration: any) => {
      try {
        await this.storage.setItem('regId', registration.registrationId);
        console.log('user registration id saved on local storage: ', registration.registrationId);
      } catch (error) {
        console.log(error);
      }
    });
    this.pushNotificationObject.on('error').subscribe(error => alert('Error with Push plugin' + error));

    return this.pushNotificationObject.on('notification');

  }
}
