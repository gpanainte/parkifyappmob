import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../app/user.service';
import { HomePage } from '../home/home';
import { NativeStorage } from '@ionic-native/native-storage';



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  // @ViewChild(Nav) nav: Nav;
  // rootPage: any;
  public username: string;
  public password: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserService) {
    //  this.rootPage = this;
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async login(username: string, password: string): Promise<void> {
    this.userService.login(username, password).then(()=>{
      this.navCtrl.setRoot(HomePage);
    });
  }

}
