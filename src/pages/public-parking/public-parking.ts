import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the PublicParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-public-parking',
  templateUrl: 'public-parking.html',
})
export class PublicParkingPage {
  @Input() notification:any;
  notifData : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.notifData = navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PublicParkingPage');
  }

}
