import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ParkingDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-parking-details',
  templateUrl: 'parking-details.html',
})
export class ParkingDetailsPage {
  @Input() notification:any;
  notifData : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.notifData = this.notification.additionalData;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParkingDetailsPage');
  }

}
