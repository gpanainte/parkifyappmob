import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserService } from './user.service';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PushNotificationProvider } from '../providers/push-notification/push-notification';
import { PushObject } from '@ionic-native/push';
import { PublicParkingPage } from '../pages/public-parking/public-parking';


@Component({
  templateUrl: 'app.html'
})
export class parkifyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any, icon: string }>;

  pushObject: PushObject;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private userService: UserService,
    private pushNotificationService: PushNotificationProvider,
    private alertCtrl: AlertController

  ) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: 'home' },
      // { title: 'List', component: ListPage, icon: '' },
      // { title: 'Login', component: LoginPage, icon: '' },
      // { title: 'Payment Settings', component: PaymentSettingsPage, icon: '' },
      // { title: 'Parking List', component: ParkingListPage, icon: '' },
      // { title: 'Parking Details', component: ParkingDetailsPage, icon: '' },
      // { title: 'My Parkings', component: MyParkingsPage, icon: '' },
      // { title: 'Current Parking', component: CurrentParkingPage, icon: '' },
      // { title: 'Payemnt History', component: PaymentHistoryPage, icon: '' },
      // { title: 'Account Preferences', component: UserDetailsPage, icon: '' }

    ];

  }

  initializeApp() {
    this.platform.ready().then(async () => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      const pushNotification = await this.pushNotificationService.initializePushNotifications();
      pushNotification.subscribe(notification => {
        this.processNotification(notification);
      });
      this.userService.isLoggedin().then(async (result) => {
        if (result) {
          this.rootPage = HomePage;
        } else {
          this.rootPage = LoginPage;
        }

      });

    });
  }
  processNotification(notification: any): void {
    const eventId = notification.additionalData.parkingEventId;
    const notificationData = notification.additionalData;
    console.log("i have a parking event id of: ", notification);
    if (notification.additionalData.eventType == 'PUBLIC') {
      this.processParkingNotification(eventId, notificationData);
    }

    if (notification.additionalData.eventType == 'PRIVATE') {
      this.processParkingNotification(eventId, notificationData);
    }

    if (notification.additionalData.eventType == 'PAYMENT') {
      this.processParkingPaymentNotification(eventId, notificationData);
    }

  }

  private processParkingNotification(eventId: string, notificationData: any): void {

    let alert = this.alertCtrl.create({
      title: "You have parked on a " + notificationData.eventType + " spot",
      subTitle: "Parking is available Mon-Sun from: " + notificationData.openFrom + " - " + notificationData.openTo,
      message: "You will be charged with: " + notificationData.hourlyFee + " " + notificationData.currency + "/hour",
      buttons: [{
        text: 'Autocharge',
        handler: () => {
          // user has clicked the alert button
          // begin the alert's dismiss transition
          let navTransition = alert.dismiss();

          // start some async method
          this.userService.acceptPayment(eventId).then(() => {
            // once the async operation has completed
            // then run the next nav transition after the
            // first transition has finished animating out

            navTransition.then(() => {
              // this.nav.setRoot(HomePage);
              this.nav.setRoot(PublicParkingPage, notificationData)
            });
          });
          return false;
        }

      }, {
        text: 'View details',
        handler: () => {
          console.log("view details")
        }
      }
      ]
    });
    alert.present();
  }

  private processParkingPaymentNotification(eventId: number, notificationData: any): void {

    let alert = this.alertCtrl.create({
      title: "Thank you for parking with us!",
      subTitle: " You are charged with " + notificationData.amount + notificationData.currency + " for " + notificationData.quantity + " hours",
      message: "Enjoy your day",
      buttons: [{
        text: 'Ok',
        handler: ()=>{
          let navTransition = alert.dismiss();

          navTransition.then(() => {
            // this.nav.setRoot(HomePage);
            this.nav.setRoot(HomePage)
          });

          return false;

        }
      }
      ]
    });
    alert.present();
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}





