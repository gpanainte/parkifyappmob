import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { NativeStorage } from '@ionic-native/native-storage';
import { AppConfig } from './config.constants';
import { UserModel } from './models/user.model';
import { UpdateUserDto } from './dto/update-user.dto';


@Injectable()
export class UserService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private serverUrl = AppConfig.backendServer;
    private username: string;
    user: UserModel;


    constructor(private http: Http, private storage: NativeStorage) { }


    async login(username: string, password: string): Promise<void> {
        
        await this.setUsername(username);
        await this.getUserProfile(username);
        await this.updateUserRegistrationId(this.user)
        console.log('user: ', this.user);

    }
    async updateUserRegistrationId(user: UserModel) {
        const registrationId = await this.storage.getItem('regId');
        const updateUserDto: UpdateUserDto = {
          name: user.name,
          plateNo: user.plateNo,
          tagId: user.tagId,
          registrationId: registrationId
        };

        const result = await this.updateUser(updateUserDto);
        console.log('result of userUpdate', result);
    }

    private async getUserProfile(username: string): Promise<UserModel> {
        if (this.user === undefined) {
            const user = await this.http.get(AppConfig.jsBackendServer + `/user/${username}/profile`).toPromise();
            this.user = user.json();
        }

        return this.user;
    }

    private async setUsername(username) {
        await this.storage.setItem('user', username);
        this.username = username;
    }

    getUsername(): string {
        return this.username;
    }
    async isLoggedin(): Promise<boolean> {
        console.log('is user logged?');
        try {
            const username = await this.storage.getItem('user');
            console.log('user from local storage', username);
            if (username !== undefined) {
                console.log('user is logged: ', username);
                const user = await this.getUserProfile(username);
                await this.updateUserRegistrationId(user);
            }
            return username !== undefined;
        } catch (error) {
            return false;
        }
    }

    private handleError(error: any): void {
        console.log("Error occured in UserService", error);
    }

    acceptPayment(parkingEventId: string): Promise<string> {

        console.log("confirm payment: ", parkingEventId);
        return this.http.post(AppConfig.jsBackendServer + `/parkings/parkingEvent/${parkingEventId}/accept`, {}, { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    async updateUser(updateUserDto: UpdateUserDto){
        return await this.http.put(AppConfig.jsBackendServer+`/user/${this.user.id}`,updateUserDto).toPromise();
    }


}
