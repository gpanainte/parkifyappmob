export class UpdateUserDto {
     name: string;
     plateNo: string;
     tagId: number;
     registrationId: string;
}
