import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { parkifyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { PaymentSettingsPage } from '../pages/payment-settings/payment-settings';
import { ParkingListPage } from '../pages/parking-list/parking-list';
import { ParkingDetailsPage } from '../pages/parking-details/parking-details';
import { MyParkingsPage } from '../pages/my-parkings/my-parkings';
import { CurrentParkingPage } from '../pages/current-parking/current-parking';
import { PaymentHistoryPage } from '../pages/payment-history/payment-history';
import { UserDetailsPage } from '../pages/user-details/user-details';
import { UserService } from './user.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Push } from '@ionic-native/push';
import { HttpModule } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { ParkingsService } from './parkings.service';
import { PublicParkingPage } from '../pages/public-parking/public-parking';
import { PushNotificationProvider } from '../providers/push-notification/push-notification';
import { ComponentsModule } from '../components/components.module';


@NgModule({
  declarations: [
    parkifyApp,
    HomePage,
    ListPage,
    LoginPage,
    PaymentSettingsPage,
    ParkingListPage,
    ParkingDetailsPage,
    MyParkingsPage,
    CurrentParkingPage,
    PaymentHistoryPage,
    UserDetailsPage,
    PublicParkingPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ComponentsModule,
    IonicModule.forRoot(parkifyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    parkifyApp,
    HomePage,
    LoginPage,
    ListPage,
    PaymentSettingsPage,
    ParkingListPage,
    ParkingDetailsPage,
    MyParkingsPage,
    CurrentParkingPage,
    PaymentHistoryPage,
    UserDetailsPage,
    PublicParkingPage
  ],
  providers: [

    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Push,
    UserService,
    NativeStorage,
    // AlertService,
    ParkingsService,
    PushNotificationProvider

  ]
})
export class AppModule { }
