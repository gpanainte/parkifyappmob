export class UserModel{
    id: number;
    name: string;
    plateNo: string;
    tagId: number;
    registrationId: string;
}